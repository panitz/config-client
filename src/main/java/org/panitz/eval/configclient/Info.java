package org.panitz.eval.configclient;

public class Info {
    private final String applicationName;
    private final String message;
    private final String username;
    private final String password;

    public Info(String applicationName, String message, String username, String password) {
        this.applicationName = applicationName;
        this.message = message;
        this.username = username;
        this.password = password;
    }


    public String getApplicationName() {
        return applicationName;
    }

    public String getMessage() {
        return message;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
