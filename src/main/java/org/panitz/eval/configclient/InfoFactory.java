package org.panitz.eval.configclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

@Service
@RefreshScope
public class InfoFactory {

    @Value("${spring.application.name}")
    private String applicationName;
    @Value("${config.message}")
    private String message;
    @Value("${config.credentials.username}")
    private String username;
    @Value("${config.credentials.password}")
    private String password;

    Info createInfo() {
        return new Info(applicationName, message, username, password);
    }
}
