MVN_VERSION := $(shell mvn -q -Dexec.executable="echo" -Dexec.args='$${project.version}' --non-recursive exec:exec)
MVN_ARTEFACT := $(shell mvn -q -Dexec.executable="echo" -Dexec.args='$${project.name}' --non-recursive exec:exec)
IMAGE=panitz/${MVN_ARTEFACT}:${MVN_VERSION}

build_image:
	docker build -t ${IMAGE} .

push_image: build_image
	docker push ${IMAGE}
