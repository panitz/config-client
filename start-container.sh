#!/bin/sh

MVN_VERSION=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.version}' --non-recursive exec:exec)
MVN_ARTEFACT=$(mvn -q -Dexec.executable="echo" -Dexec.args='${project.name}' --non-recursive exec:exec)
IMAGE="panitz/$MVN_ARTEFACT:$MVN_VERSION"

docker network create spring 2>/dev/null && true
docker rm config-client --force 2>/dev/null && true
docker run -d --rm \
  --name config-client \
  --network="spring" \
  -p 8080:8080 \
  -e SPRING_PROFILES_ACTIVE=dev \
  -e SPRING_CLOUD_CONFIG_ENABLED=true \
  -e SPRING_CLOUD_CONFIG_USERNAME=user \
  -e SPRING_CLOUD_CONFIG_PASSWORD=password \
  -e SPRING_CLOUD_CONFIG_URI=http://config-server:8888 \
  -e SPRING_RABBITMQ_HOST=rabbitmq \
  -e SPRING_RABBITMQ_PORT=5672 \
  -e SPRING_RABBITMQ_VIRTUALHOST=/ \
  -e SPRING_RABBITMQ_USERNAME=user \
  -e SPRING_RABBITMQ_PASSWORD=password \
  $IMAGE